class MovieController < ApplicationController
  def index
    @movies = Movie.includes(:reviews).actor_search(params[:actor])
  end
end
