class ImporterController < ApplicationController
  def import_file
  end

  def parse_csv
    @result = CsvDb.convert_save(params[:dump][:model_name],params[:dump][:file])
    
    if @result[:success]
      flash[:notice] = @result[:message]
      redirect_to action: :import_file, notice: "CSV imported successfully!"
     else
      flash[:error] = @result[:message]
      redirect_to action: 'import_file'
     end
     
  end
end
