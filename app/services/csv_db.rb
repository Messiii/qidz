require 'csv'
class CsvDb
 class << self
   def convert_save(model_name, csv_data)
     csv_file = csv_data.read
     @result = {success: false, message: []}
     CSV.parse(csv_file, headers: true) do |row|
        target_model = model_name.classify.constantize

        new_hash = {}
        row.to_hash.each_pair do |k,v|
         new_hash.merge!({k.parameterize.underscore => v}) 
        end

        if target_model == Review
            obj_movie = Movie.find_by(movie: new_hash["movie"])
            obj_user = User.find_by(name: new_hash["user"])
            @result[:message] << "Movie #{new_hash["movie"]} not found" if obj_movie.nil?
            @result[:message] << "User #{new_hash["user"]} not found" if obj_user.nil?
            next if obj_movie.nil? || obj_user.nil?
            new_hash["movie_id"] = new_hash.delete "movie"; new_hash["movie_id"] = obj_movie.id
            new_hash["user_id"] = new_hash.delete "user"; new_hash["user_id"] = obj_user.id

        end
        target_model.create!(new_hash)

     end
     @result[:message] << "CSV imported successfully!"
     @result[:success] = true
     @result
   end
 end
end

