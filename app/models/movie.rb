class Movie < ApplicationRecord
    has_many :reviews
    scope :actor_search, -> (actor) {where("lower(actor) LIKE ?", "%#{actor.try(:downcase)}%")}
end
