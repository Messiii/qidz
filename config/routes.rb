Rails.application.routes.draw do
  get 'movie/index'
  get 'importer', to: 'importer#import_file'
  post 'importer', to: 'importer#parse_csv'
end
